<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        
        <link href="{{ asset('frontend/css/bootstrap.min.css') }}" rel="stylesheet">
        <link href="{{ asset('frontend/css/font-awesome.min.css') }}" rel="stylesheet">
        <link href="{{ asset('frontend/css/prettyPhoto.css') }}" rel="stylesheet">
        <link href="{{ asset('frontend/css/price-range.css') }}" rel="stylesheet">
        <link href="{{ asset('frontend/css/animate.css') }}" rel="stylesheet">
        <link href="{{ asset('frontend/css/main.css') }}" rel="stylesheet">
        <link href="{{ asset('frontend/css/responsive.css') }}" rel="stylesheet">
        <link href="{{ asset('frontend/css/rate.css') }}" rel="stylesheet" >
    </head>
    <body>
       <div class="demo"></div>
        <p>Thông tin khách hàng:</p>
        <p>Full name: {{ Auth::user()->name }}</p>
        <p>Phone: {{ Auth::user()->phone }}</p>
        <p>Address: {{ Auth::user()->address }}</p>
    	<p></p>
    	<p></p>
    	<p>Thong tin giỏ hàng:</p>
        <section id="cart_items">
		<div class="container">
			<div class="table-responsive cart_info">
				<table class="table table-condensed">
					<thead>
						<tr class="cart_menu">
							<td class="image">Item</td>
							<td class="description"></td>
							<td class="price">Price</td>
							<td class="quantity">Quantity</td>
							<td class="total">Total</td>
							<td></td>
						</tr>
					</thead>
					<tbody>
                        @foreach($data_cart as $values)
                        <?php  $values['image']=json_decode($values['image']); ?>
                        <tr>
							<td class="cart_product">
								<a href=""><img src="{{ asset('upload/user/product/hinh50_'.$values['image'][0]) }}" alt=""></a>
							</td>
							<td class="cart_description">
								<h4><a href="">{{$values['name']}}</a></h4>
								<p class='id'>{{$values['id']}}</p>
							</td>
							<td class="cart_price">
								<p><?php echo (int)$values['price'] - (int)$values['price']*$values['sale']/100 ?>$</p>
							</td>
							<td class="cart_quantity">
								<div class="cart_quantity_button">
									<a class="cart_quantity_up"  > + </a>
									<input class="cart_quantity_input" type="text" name="quantity" value="{{$values['qty']}}" autocomplete="off" size="2">
									<a class="cart_quantity_down"> - </a>
								</div>
							</td>
							<td class="cart_total">
								<p class="cart_total_price">
									<?php 
										$price=((int)$values['price'] - (int)$values['price']*$values['sale']/100)*$values['qty'];
										echo  $price;
									?>$
								</p>
							</td>
							<td class="cart_delete">
								<a class="cart_quantity_delete"><i class="fa fa-times"></i></a>
							</td>
						</tr>
                        @endforeach

					</tbody>
				</table>
			</div>
            <div class="col-sm-6">
					<div class="total_area">
						<ul>
							<li>Cart Sub Total <span id='all_total'>{{$sum}}</span></li>
							<li>Eco Tax <span>$0</span></li>
							<li>Shipping Cost <span>Free</span></li>
							<li>Total <span>{{$sum}}</span></li>
						</ul>
							<a class="btn btn-default update" href="">Update</a>
							<a class="btn btn-default check_out" href="">Check Out</a>
					</div>
				</div>
		</div>
	</section> <!--/#cart_items-->
    </body>
</html>