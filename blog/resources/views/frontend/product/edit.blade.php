@extends('frontend.layouts.app2')
@section('content')
                <div class="col-sm-3">
					<div class="left-sidebar">
						<h2>Category</h2>
						<div class="panel-group category-products" id="accordian"><!--category-productsr-->
                            <div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
                                        <li>
                                            <a href="{{ url ('/profile')}}">Account</a>
                                            <span class="badge pull-right"><i class="fa fa-plus"></i></span>
                                        </li>
									</h4>
								</div>
							</div>
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
                                        <li>
                                            <a href="{{ url ('/account/product')}}">Product</a>
                                            <span class="badge pull-right"><i class="fa fa-plus"></i></span>
                                        </li>
									</h4>
								</div>
							</div>
						</div><!--/category-products-->
					</div>
				</div>
                <div class="col-sm-9 padding-right">
                    <h2 class="title text-center">Profile</h2>
						
                    <div class="col-sm-9 col-sm-offset-1">
                        <div class="login-form"><!--login form-->
                            <form method="post" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <input name="name" type="text" value="{{ $data_product[0]->name }}" class="form-control form-control-line">
                                <input name="price" type="text"  value="{{ $data_product[0]->price }}" class="form-control form-control-line" >
                                <select name="id_category" class="form-control form-control-line" >                               
                                        @foreach ($data_category as $item)
                                        <option  value="{{ $item->id }}" {{ ( $item->id == $data_product[0]->id_category) ? 'selected' : '' }}> {{ $item->category }} </option>
                                        @endforeach
                                </select>
                                <p></p>
                                <select name="id_brand" class="form-control form-control-line" >
                                        @foreach ($data_brand as $item)
                                        <option  value="{{ $item->id }}" {{ ( $item->id == $data_product[0]->id_brand) ? 'selected' : '' }}> {{ $item->brand }}</option>
                                        @endforeach
                                </select>
                                <p></p>
                                <select id="status" name="status"  class="form-control form-control-line" onchange="changeFunc(value);" >
                                    <option  value="0" {{ ( $data_product[0]->status==0) ? 'selected' : '' }}> New </option>
                                    <option  value="1" {{ ( $data_product[0]->status==1) ? 'selected' : '' }}> Sales </option>
                                </select>
                                <p></p>
                                <input id="sale" value="{{ ( $data_product[0]->sale)}}" name="sale" type="{{( $data_product[0]->status==1 ?'text' : 'hidden' )}}"  class="form-control form-control-line" >
                                <input name="company" type="text"  value="{{ $data_product[0]->company }}" class="form-control form-control-line" >
                                <input type="file" name="image[]" multiple >
                                <table>
                                    <tr>
                                    @foreach($data_product[0]->image as $value)                                 
                                            <th><img class="media-object" src="{{ asset('upload/user/product/hinh200_'.$value)}}" alt=""></th>                                    @endforeach
                                    </tr>
                                    <tr>
                                    @foreach($data_product[0]->image as $value)
                                            <th><input name="image_delete[]" type="checkbox" value="{{$value}}"></th>
                                    @endforeach
                                    </tr>
                                </table>
                                <textarea name="details"  rows="11">{{ $data_product[0]->details }}</textarea>	
                                @if(session('success'))
								<div class="alert alert-success alert dismissible">
									<button type="close" class="close" data-dismiss="alert" aria-hidden="true">x</button>
									<h4><i class="icon fa fa-check"></i>Thông báo</h4>
									{{session('success')}}
								</div>
							    @endif
                                <div class="col-md-12">
                                    @if($errors->any())
                                    <ul>
                                        @foreach($errors->all() as $error)
                                            <li>{{$error}}</li>
                                        @endforeach
                                    </ul>
                                    @endif
                                </div>
                                <button type="submit" class="btn btn-default">Update Product</button>
                            </form>
                        </div><!--/login form-->
                    </div>
@endsection
<script src="{{ asset('frontend/js/jquery-1.9.1.min.js') }}"></script>

	<script>

        function changeFunc(i) {
            if(i==1){
                $('input#sale').attr('type','text');
            }else{
                $('input#sale').attr('type','hidden');
                $('input#sale').val(0);
            }
        }
    </script>