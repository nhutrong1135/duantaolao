@extends('frontend.layouts.app2')
@section('content')
	<section id="cart_items">	
			<div class="col-sm-3">
					<div class="left-sidebar">
						<h2>Category</h2>
						<div class="panel-group category-products" id="accordian"><!--category-productsr-->
                            <div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
                                        <li>
                                            <a href="{{ url ('/profile')}}">Account</a>
                                            <span class="badge pull-right"><i class="fa fa-plus"></i></span>
                                        </li>
									</h4>
								</div>
							</div>
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
                                        <li>
                                            <a href="{{ url ('/account/product')}}">Product</a>
                                            <span class="badge pull-right"><i class="fa fa-plus"></i></span>
                                        </li>
									</h4>
								</div>
							</div>
						</div><!--/category-products-->
					</div>
				</div>
				<div class="col-sm-9 padding-right">
                    <h2 class="title text-center">Product</h2>
					@if(count($data_product)>0)
					<div class="table-responsive cart_info">
						<table class="table table-condensed">
							<thead>
								<tr class="cart_menu">
									<td class="id">Id</td>
									<td class="name">Name</td>
									<td class="price">Image</td>
									<td class="price">Price</td>
									<td class="action">Action</td>
									<td></td>
								</tr>
							</thead>
							<tbody>
								@foreach($data_product as $value )
									<tr>
										<td class="cart_description">
											<a href="">{{$value->id}}</a>
										</td>
										<td class="cart_description">
											<a href="">{{$value->name}}</a>
										</td>
										<td class="cart_product">
											<a href=""><img   src="{{ asset('upload/user/product/hinh50_'.$value->image[0]) }}" alt=""></a>
										</td>
										<td class="cart_total">
											<p class="cart_total_price"><?php echo (int)$value->price - (int)$value->price*$value->sale/100 ?> $</p>
										</td>
										<td class="cart_delete">
											<ul><a class="cart_quantity_delete" href="{{ url('/account/product/edit/'.$value->id) }}"><i class="fa fa-cog fa-fw"></i>Edit</a></ul>
											<ul><a class="cart_quantity_delete" href="{{ url('/account/product/delete/'.$value->id) }}"><i class="fa fa-times"></i>Delete</a></ul>
										</td>
									</tr>
								@endforeach
							</tbody>
						</table>					
					</div>			
					@endif
					
								
					@if(count($data_product)==0)
						<h2 class="title text-center">Không có sản phẩm nào</h2>
					@endif
					@if(session('success'))
						<div class="alert alert-success alert dismissible">
							<button type="close" class="close" data-dismiss="alert" aria-hidden="true">x</button>
							<h4><i class="icon fa fa-check"></i>Thông báo</h4>
							{{session('success')}}
						</div>
				    @endif
					<a class="btn btn-primary" href="{{ url('/account/product/add') }}">Add New</a>
				</div>

	</section>
                
						
@endsection