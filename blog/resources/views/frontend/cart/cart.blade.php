@extends('frontend.layouts.app2')
@section('content')
<section>
<?php $total=0;?>
<div class="row">

				<div class="col-sm-4 col-sm-offset-1">
					<div class="signup-form"><!--sign up form-->

						<?php
							if(!Auth::check()){
						?>
						<h2>New User Signup!</h2>
						<form method="post" action="{{ url('/cart/order')}}" >
							{{ csrf_field() }}
							<input name="name" type="text" placeholder="Name"/>
							<input name="email" type="email" placeholder="Email Address"/>
							<input name="password" type="password" placeholder="Password"/>
							<input name="phone" type="text" placeholder="Phone"/>
							<input name="address" type="text" placeholder="Address"/>
							<div class="form-group">
								@if($errors->any())
									<ul>
									@foreach($errors->all() as $error)
										<li>{{$error}}</li>
									@endforeach
									</ul>
								@endif
							</div>
							<button type="submit" class="btn btn-default">Register and Order</button>
						</form>
						<?php
							}
						?>
						<?php
							if(Auth::check()){
						?>
						<form method='post' action="{{ url('/cart/order')}}">
							{{ csrf_field() }}
							<input name="name" type="hidden" value="Name"/>
							<input name="email" type="hidden" value="e@gmail.com"/>
							<input name="password" type="hidden" value="Password"/>
							<input name="phone" type="hidden" value="Phone"/>
							<input name="address" type="hidden" value="Address"/>
							<button type="submit" class="btn btn-default">Order</button>
						</form>
						<?php
							}
						?>
					</div><!--/sign up form-->
				</div>
    <section id="cart_items">
		<div class="container">
			<div class="table-responsive cart_info">
				<table class="table table-condensed">
					<thead>
						<tr class="cart_menu">
							<td class="image">Item</td>
							<td class="description"></td>
							<td class="price">Price</td>
							<td class="quantity">Quantity</td>
							<td class="total">Total</td>
							<td></td>
						</tr>
					</thead>
					<tbody>
                        @foreach($data_cart as $values)
                        <tr>
							<td class="cart_product">
								<a href=""><img src="{{ asset('upload/user/product/hinh50_'.$values[0]->image[0]) }}" alt=""></a>
							</td>
							<td class="cart_description">
								<h4><a href="">{{$values[0]->name}}</a></h4>
								<p class='id'>{{$values[0]->id}}</p>
							</td>
							<td class="cart_price">
								<p><?php echo (int)$values[0]->price - (int)$values[0]->price*$values[0]->sale/100 ?>$</p>
							</td>
							<td class="cart_quantity">
								<div class="cart_quantity_button">
									<a class="cart_quantity_up"  > + </a>
									<input class="cart_quantity_input" type="text" name="quantity" value="{{$values[0]->qty}}" autocomplete="off" size="2">
									<a class="cart_quantity_down"> - </a>
								</div>
							</td>
							<td class="cart_total">
								<p class="cart_total_price">
									<?php 
										$price=((int)$values[0]->price - (int)$values[0]->price*$values[0]->sale/100)*$values[0]->qty;
										$total+=$price;
										echo  $price;
									?>$
								</p>
							</td>
							<td class="cart_delete">
								<a class="cart_quantity_delete"><i class="fa fa-times"></i></a>
							</td>
						</tr>
                        @endforeach

					</tbody>
				</table>
				
				@if(session('success'))
					<div class="alert alert-success alert dismissible">
						<button type="close" class="close" data-dismiss="alert" aria-hidden="true">x</button>
						<h4><i class="icon fa fa-check"></i>Thông báo</h4>
						{{session('success')}}
					</div>
			    @endif
			</div>
		</div>
	</section> <!--/#cart_items-->

	<section id="do_action">
		<div class="container">
			<div class="heading">
				<h3>What would you like to do next?</h3>
				<p>Choose if you have a discount code or reward points you want to use or would like to estimate your delivery cost.</p>
			</div>
			<div class="row">
				<div class="col-sm-6">
					<div class="chose_area">
						<ul class="user_option">
							<li>
								<input type="checkbox">
								<label>Use Coupon Code</label>
							</li>
							<li>
								<input type="checkbox">
								<label>Use Gift Voucher</label>
							</li>
							<li>
								<input type="checkbox">
								<label>Estimate Shipping & Taxes</label>
							</li>
						</ul>
						<ul class="user_info">
							<li class="single_field">
								<label>Country:</label>
								<select>
									<option>United States</option>
									<option>Bangladesh</option>
									<option>UK</option>
									<option>India</option>
									<option>Pakistan</option>
									<option>Ucrane</option>
									<option>Canada</option>
									<option>Dubai</option>
								</select>
								
							</li>
							<li class="single_field">
								<label>Region / State:</label>
								<select>
									<option>Select</option>
									<option>Dhaka</option>
									<option>London</option>
									<option>Dillih</option>
									<option>Lahore</option>
									<option>Alaska</option>
									<option>Canada</option>
									<option>Dubai</option>
								</select>
							
							</li>
							<li class="single_field zip-field">
								<label>Zip Code:</label>
								<input type="text">
							</li>
						</ul>
						<a class="btn btn-default update" href="">Get Quotes</a>
						<a class="btn btn-default check_out" href="">Continue</a>
					</div>
				</div>
				<div class="col-sm-6">
					<div class="total_area">
						<ul>
							<li>Cart Sub Total <span id='all_total'>{{$total}}</span></li>
							<li>Eco Tax <span>$0</span></li>
							<li>Shipping Cost <span>Free</span></li>
							<li>Total <span>{{$total}}</span></li>
						</ul>
							<a class="btn btn-default update" href="">Update</a>
							<a class="btn btn-default check_out" href="">Check Out</a>
					</div>
				</div>
			</div>
		</div>
	</section><!--/#do_action-->

					
</div>
</section>
                
						
@endsection
<script src="{{ asset('frontend/js/jquery-1.9.1.min.js') }}"></script>

	<script>
        $(document).ready(function(){
			$('a.cart_quantity_up').click(function(){
				var qty=$(this).closest("tr").find("input.cart_quantity_input").val()
				var id_product=$(this).closest("tr").find("p.id").text()
				var price=$(this).closest("tr").find("td.cart_price p").text().replace("$","")
				var total=$('span#all_total').text();
				qty=parseInt(qty);
				$.ajax({
						type:'POST',
						url:"{{ route('ajaxQty.post') }}",
						data:{
							id_product:id_product,
							qty:qty+1,
							_token: '{{csrf_token()}}',
						},
						success:function(data){
							console.log(data.success);
						},
						error: function() {
                       		console.log(error);
                   		}
					});

				$(this).closest("tr").find("input.cart_quantity_input").val(parseInt(qty)+1)
				$(this).closest("tr").find("p.cart_total_price").text(parseInt(price)*(parseInt(qty)+1)+'$')
				$('span#all_total').text(parseInt(total)+parseInt(price));
				
			})
			$('a.cart_quantity_down').click(function(){
				var qty=$(this).closest("tr").find("input.cart_quantity_input").val()
				var id_product=$(this).closest("tr").find("p.id").text()
				var price=$(this).closest("tr").find("td.cart_price p").text().replace("$","")
				var total=$('span#all_total').text();
					$.ajax({
						method: "POST",// phương thức dữ liệu được truyền đi
						url: "{{ route('ajaxQty.post') }}",// gọi đến file server show_data.php để xử lý
						data: 
						{
							id_product: id_product,
							qty: qty-1,
							_token: '{{csrf_token()}}',                  
						},
						success:function(data){
							console.log(data.success);
						},
						error: function() {
                       		console.log(error);
                   		}
					});


				if(qty>1){
					$(this).closest("tr").find("input.cart_quantity_input").val(qty-1)
					$(this).closest("tr").find("input.cart_quantity_input").val(parseInt(qty)-1)
					$(this).closest("tr").find("p.cart_total_price").text(parseInt(price)*(parseInt(qty)-1)+'$')
				}
				else{
					$(this).closest("tr").remove();
				}
				$('span#all_total').text(parseInt(total)-parseInt(price));
			})
			$("a.cart_quantity_delete").click(function(){
				var qty=$(this).closest("tr").find("input.cart_quantity_input").val()
				var id_product=$(this).closest("tr").find("p.id").text()
				var price=$(this).closest("tr").find("td.cart_price p").text().replace("$","")
				var total=$('span#all_total').text();
					$.ajax({
						type:'POST',
						url:"{{ route('ajaxQty.post') }}",
						data:{
							id_product:id_product,
							qty:0,
							_token: '{{csrf_token()}}',
						},
						success:function(data){
							console.log(data.success);
						},
						error: function() {
                       		console.log(error);
                   		}
					});
				$('span#all_total').text(parseInt(total)-(parseInt(price)*(parseInt(qty))));
				$(this).closest("tr").remove();
			})

		})
    </script>