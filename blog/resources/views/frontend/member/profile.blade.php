@extends('frontend.layouts.app2')
@section('content')
    <?php
        $data = Auth::user();

    ?>
                <div class="col-sm-3">
					<div class="left-sidebar">
						<h2>Category</h2>
						<div class="panel-group category-products" id="accordian"><!--category-productsr-->
                            <div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
                                        <li>
                                            <a href="{{ url ('/profile')}}">Account</a>
                                            <span class="badge pull-right"><i class="fa fa-plus"></i></span>
                                        </li>
									</h4>
								</div>
							</div>
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
                                        <li>
                                            <a href="{{ url ('/account/product')}}">Product</a>
                                            <span class="badge pull-right"><i class="fa fa-plus"></i></span>
                                        </li>
									</h4>
								</div>
							</div>
						</div><!--/category-products-->
					</div>
				</div>
                <div class="col-sm-9 padding-right">
                    <h2 class="title text-center">Profile</h2>
						
                    <div class="col-sm-9 col-sm-offset-1">
                        <div class="login-form"><!--login form-->
                            <form method="post" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <input name="name" type="text"  value="<?php echo $data->name?>" class="form-control form-control-line">
                                <input name="email" type="email" readonly  placeholder="johnathan@admin.com" value="<?php echo $data->email?>" class="form-control form-control-line" name="example-email" id="example-email">
                                <input name="passwordnew" type="password" value="password"  class="form-control form-control-line">
                                <input name="passwordnewcorfirm" type="password" value="password" class="form-control form-control-line">
                                <input name="phone" type="text" placeholder="+123 5648985" value="<?php echo $data->phone?>" class="form-control form-control-line">
                                <input name="address" type="text" placeholder="YongChi-Tokyo" value="<?php echo $data->address?>" class="form-control form-control-line">
                                <input type="file" name="avatar" >
                                <select name="country" class="form-control form-control-line" >
                                    <option>Select Item(*)</option>
                                        @foreach ($data_country as $item)
                                        <option value="{{ $item->id }}" {{ ( $item->id == $data->country) ? 'selected' : '' }}> {{ $item->name }} </option>
                                        @endforeach
                                </select>
                                <div class="col-md-12">
                                    @if(!empty($successMessage = \Illuminate\Support\Facades\Session::get('success')))
                                        {{ $successMessage }}
                                    @endif
                                </div>
                                <div class="col-md-12">
                                    @if($errors->any())
                                    <ul>
                                        @foreach($errors->all() as $error)
                                            <li>{{$error}}</li>
                                        @endforeach
                                    </ul>
                                    @endif
                                </div>
                                <button type="submit" class="btn btn-default">Update Profile</button>
                            </form>
                        </div><!--/login form-->
                    </div>
@endsection