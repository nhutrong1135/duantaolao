@extends('frontend.layouts.app2')
@section('content')
                <div class="col-sm-4 col-sm-offset-1">
					<div class="login-form"><!--login form-->
						<h2>Login to your account</h2>
						<form method="post">
							{{ csrf_field() }}
							<input name='email' type="email" placeholder="Email" />
							<input name='password' type="password" placeholder="Password" />
							<span>
								<input type="checkbox" class="checkbox"> 
								Keep me signed in
							</span>
							<div class="form-group">
								@if($errors->any())
									<ul>
									@foreach($errors->all() as $error)
										<li>{{$error}}</li>
									@endforeach
									</ul>
								@endif
							</div>
							<button type="submit" class="btn btn-default">Login</button>
						</form>
					</div><!--/login form-->
				</div>
@endsection