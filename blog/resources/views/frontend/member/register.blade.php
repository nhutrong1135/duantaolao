@extends('frontend.layouts.app2')
@section('content')
                <div class="col-sm-4 col-sm-offset-1">
					<div class="signup-form"><!--sign up form-->
						<h2>New User Signup!</h2>
						<form method="post" >
							{{ csrf_field() }}
							<input name="name" type="text" placeholder="Name"/>
							<input name="email" type="email" placeholder="Email Address"/>
							<input name="password" type="password" placeholder="Password"/>
							<input name="phone" type="text" placeholder="Phone"/>
							<input name="address" type="text" placeholder="Address"/>
							<div class="form-group">
								@if($errors->any())
									<ul>
									@foreach($errors->all() as $error)
										<li>{{$error}}</li>
									@endforeach
									</ul>
								@endif
							</div>
							@if(session('success'))
								<div class="alert alert-success alert dismissible">
									<button type="close" class="close" data-dismiss="alert" aria-hidden="true">x</button>
									<h4><i class="icon fa fa-check"></i>Thông báo</h4>
									{{session('success')}}
								</div>
							@endif
							<button type="submit" class="btn btn-default">Signup</button>
						</form>
					</div><!--/sign up form-->
				</div>
@endsection