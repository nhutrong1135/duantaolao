@extends('frontend.layouts.app')
@section('content')
                <div class="col-sm-9">
					<div class="blog-post-area">
						<h2 class="title text-center">Latest From our Blog</h2>
						@foreach($data_blog as $value)
						<div class="single-blog-post">
							<h3>{{$value->title}}</h3>
							<div class="post-meta">
								<ul>
									<li><i class="fa fa-user"></i> Mac Doe</li>
									<li><i class="fa fa-clock-o"></i> 1:33 pm</li>
									<li><i class="fa fa-calendar"></i> {{$value->id}}</li>
								</ul>
								<span>
										<i class="fa fa-star"></i>
										<i class="fa fa-star"></i>
										<i class="fa fa-star"></i>
										<i class="fa fa-star"></i>
										<i class="fa fa-star-half-o"></i>
								</span>
							</div>
							<a href="">
								<img width="300px" height="350px" src="{{ asset('upload/user/image/'.$value->image) }}" alt="">
							</a>
								{{$value->description}}
							<a  class="btn btn-primary" href="{{ url('/blog/single/'.$value->id) }}">Read More</a>
						</div>
						@endforeach
						<div class="pagination-area">
							<ul class="pagination">
							{{$data_blog->links()}}
							</ul>
						</div>
					</div>
				</div>
@endsection