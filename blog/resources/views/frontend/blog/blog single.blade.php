@extends('frontend.layouts.app')
@section('content')
                <div class="col-sm-9">
					<div class="blog-post-area">
						<h2 class="title text-center">Latest From our Blog</h2>
						<div class="single-blog-post">
							<h3>{{$data_blog->title}}</h3>
							<div class="post-meta">
								<ul>
									<li><i class="fa fa-user"></i> Mac Doe</li>
									<li><i class="fa fa-clock-o"></i> 1:33 pm</li>
									<li><i class="fa fa-calendar"></i> {{$data_blog->id}} </li>
								</ul>
								<span>
									@for($i=1;$i<=5;$i++)
										@if($i<=round($data_rate))
											<i class="fa fa-star"></i>
										@endif
										@if($i>round($data_rate))
											<i class="fa fa-star-o"></i>
										@endif
									@endfor
									
								</span>
							</div>
							<a href="">
								<img src="{{ asset('upload/user/image/'.$data_blog->image ) }}" alt="">
							</a>
                                <?php echo $data_blog->content?>
							<div class="pager-area">
								<ul class="pager pull-right">
									@if($next)
										<li><a href="{{ $next }}">Next</a></li>
									@endif
									@if($prev)
										<li><a href="{{ $prev }}">Pre</a></li>
									@endif
									
								</ul>
							</div>
						</div>
					</div><!--/blog-post-area-->

					<div class="rating-area">
						<ul class="ratings">
							<li class="rate-this">Rate this item:</li>
							<li>
								<div class="rate">
									<div class="vote">
									@for($i=1;$i<=5;$i++)
										@if($i<=round($data_rate))
											<div class="star_{{$i}} ratings_stars ratings_over"><input value="{{$i}}" type="hidden"></div>
										@endif
										@if($i>round($data_rate))
											<div class="star_{{$i}} ratings_stars"><input value="{{$i}}" type="hidden"></div>
										@endif
									@endfor
									</div> 
								</div>
							</li>
							<li class="color">({{ $times }} votes)</li>
						</ul>
						<ul class="tag">
							<li>TAG:</li>
							<li><a class="color" href="">Pink <span>/</span></a></li>
							<li><a class="color" href="">T-Shirt <span>/</span></a></li>
							<li><a class="color" href="">Girls</a></li>
						</ul>
					</div><!--/rating-area-->

					<div class="socials-share">
						<a href=""><img src="{{ asset('frontend/images/blog/socials.png')}}" alt=""></a>
					</div><!--/socials-share-->

					<div class="media commnets">
						<a class="pull-left" href="#">
							<img class="media-object" src="{{ asset('frontend/images/blog/man-one.jpg')}}" alt="">
						</a>
						<div class="media-body">
							<h4 class="media-heading">Annie Davis</h4>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.  Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
							<div class="blog-socials">
								<ul>
									<li><a href=""><i class="fa fa-facebook"></i></a></li>
									<li><a href=""><i class="fa fa-twitter"></i></a></li>
									<li><a href=""><i class="fa fa-dribbble"></i></a></li>
									<li><a href=""><i class="fa fa-google-plus"></i></a></li>
								</ul>
								<a class="btn btn-primary" href="">Other Posts</a>
							</div>
						</div>
					</div><!--Comments-->
					<div class="response-area">
						<h2>3 RESPONSES</h2>
						<ul class="media-list">
							@foreach( $data_blog['comment'] as $value)
								@if($value->id_cmt_cha==0)
								<li class="media">
									<a class="pull-left" >
										<img weith="100px" height="100px" class="media-object" src="{{ asset('upload/user/avatar/'.$value->user_avatar)}}" alt="">
									</a>
									<div class="media-body">
										<ul class="sinlge-post-meta">
											<li><i class="fa fa-user"></i>{{ $value->user_name }} </li>
											<li><i class="fa fa-clock-o"></i> {{ date_format( $value->created_at,"H:i" ) }}</li>
											<li><i class="fa fa-calendar"></i> {{ date_format( $value->created_at,"D d/m/Y" ) }}</li>
										</ul>
										<p> {{$value->content}} </p>
										<input id="id_comment" type="hidden" value="{{ $value->id }}">
										<a class="btn btn-primary" id="reply"><i  class="fa fa-reply"></i>Replay</a>
									</div>
								</li>
								@endif
								@foreach($data_blog['comment'] as $value_con)
									@if($value->id==$value_con->id_cmt_cha)
									<li class="media second-media">
										<a class="pull-left" >
											<img weith="100px" height="100px" class="media-object" src="{{ asset('upload/user/avatar/'.$value_con->user_avatar)}}" alt="">
										</a>
										<div class="media-body">
											<ul class="sinlge-post-meta">
												<li><i class="fa fa-user"></i>{{ $value_con->user_name }} </li>
												<li><i class="fa fa-clock-o"></i> {{ date_format( $value_con->created_at,"H:i" ) }}</li>
												<li><i class="fa fa-calendar"></i> {{ date_format( $value_con->created_at,"D d/m/Y" ) }}</li>
											</ul>
											<p> {{$value_con->content}} </p>
										</div>
									</li>
									@endif
								@endforeach
							@endforeach
						</ul>					
					</div><!--/Response-area-->
					<div class="replay-box">
						<div class="row">
							<div class="col-sm-8">
								<div class="text-area">
									<form action="{{ url('/blog/comment') }}" method="post">
									{{ csrf_field() }}
										<div class="blank-arrow">
											<label>Your Comments</label>
										</div>
										<span>*</span>
										<input id="id_blog_reply" name="id_cmt_cha" type="hidden" value="0">
										<input name="id_blog" type="hidden" value="{{ $data_blog->id}}">
										<textarea name="content"  rows="11"></textarea>										
										<button id="comment" type="submit"  class="btn btn-primary">Post comment</button>
									</form>
								</div>
								<div class="form-group">
									<div class="col-md-12">
										@if(session('success'))
											<div class="alert alert-success alert dismissible">
												<button type="close" class="close" data-dismiss="alert" aria-hidden="true">x</button>
												<h4><i class="icon fa fa-check"></i>Thông báo</h4>
												{{session('success')}}
											</div>
										@endif
									</div>
								</div>
								<div class="form-group">
									@if($errors->any())
										<ul>
										@foreach($errors->all() as $error)
											<li>{{$error}}</li>
										@endforeach
										</ul>
									@endif
								</div>
							</div>
						</div>
					</div><!--/Repaly Box-->
				</div>
@endsection
	<script src="{{ asset('frontend/js/jquery-1.9.1.min.js') }}"></script>

	<script>
	
		$(document).ready(function(){
			//vote
			$('.ratings_stars').hover(
	            // Handles the mouseover
	            function() {
	                $(this).prevAll().andSelf().addClass('ratings_hover');
	                // $(this).nextAll().removeClass('ratings_vote'); 
	            },
	            function() {
	                $(this).prevAll().andSelf().removeClass('ratings_hover');
	                // set_votes($(this).parent());
	            }
	        );

			$('.ratings_stars').click(function(){
				var id_blog = window.location.href.substr(57,20);
				var Values =  $(this).find("input").val();
				var checkLogin = "{{Auth::check()}}";
				if (checkLogin) {
					alert("Bạn đánh giá bài viết này "+Values+" sao!!!!");
					if ($(this).hasClass('ratings_over')) {
						$('.ratings_stars').removeClass('ratings_over');
						$(this).prevAll().andSelf().addClass('ratings_over');
					} else {
						$(this).prevAll().andSelf().addClass('ratings_over');
					}
					
					$.ajax({
						type:'POST',
						url:"{{ route('ajaxRateRequest.post') }}",
						data:{
							rate:Values,
							id_blog:id_blog,
							_token: '{{csrf_token()}}',
						},
						success:function(data){
							console.log(data.success);
						},
						error: function() {
                       		console.log(error);
                   		}
					});
				} 
		    	else {
					alert("Vui lòng đăng nhập để đánh giá !!!");
				}
		    });



			$('a#reply').click(function(){
				var checkLogin = "{{Auth::check()}}";
				if (checkLogin) {
					var id_cmt_cha=$(this).closest("li.media").find('input#id_comment').val();
					$(this).closest("div.col-sm-9").find('input#id_blog_reply').val(id_cmt_cha);
					alert("Vui lòng nhập bình luận trả lời bên dưới!!!");
				}
				else{
					alert("Vui lòng đăng nhập để trả lời đánh giá !!!");
				}
			});



			$('button#comment').click(function(){
				var checkLogin = "{{Auth::check()}}";
				if (!checkLogin) {
					alert("Vui lòng đăng nhập để trả lời đánh giá !!!");
					return false;
				}
				
			});
		});
    </script>