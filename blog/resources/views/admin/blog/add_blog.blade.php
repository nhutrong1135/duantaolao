@extends('admin.layouts.app')
@section('content')
    <form method="post" class="form-horizontal form-material" enctype="multipart/form-data">
        {{ csrf_field() }}
        <div class="form-group">
            <label class="col-md-12">Title(*)</label>
            <div class="col-md-12">
                <input name="title" type="text"  class="form-control form-control-line">
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-12">Image</label>
            <div class="col-md-12">
                <input type="file" name="image" >
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-12">Description</label>
            <div class="col-md-12">
                <input name="description" type="text"  class="form-control form-control-line">
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-12">Content</label>
            <div class="col-md-12">
                <textarea rows = "10" cols = "60" name="content" id="content" type="text"  class="form-control form-control-line"></textarea>
            </div>
        </div>
        <div class="form-group">
            <div class="col-md-12">
                @if(session('success'))
					<div class="alert alert-success alert dismissible">
						<button type="close" class="close" data-dismiss="alert" aria-hidden="true">x</button>
                        <h4><i class="icon fa fa-check"></i>Thông báo</h4>
		    			{{session('success')}}
					</div>
				@endif
            </div>
        </div>
        <div class="form-group">
            @if($errors->any())
                <ul>
                @foreach($errors->all() as $error)
                    <li>{{$error}}</li>
                @endforeach
                </ul>
            @endif
        </div>
        <div class="form-group">
            <div class="col-sm-12">
                <button type="submit" class="btn btn-success">Create Blog</button>
            </div>
        </div>
      </form>
      <script src="{{ asset('ckeditor/ckeditor.js') }}"></script>
    <script> CKEDITOR.replace('content',{
        filebrowserBrowseUrl: '{{ asset('ckfinder/ckfinder.html') }}',
        filebrowserImageBrowseUrl: '{{ asset('ckfinder/ckfinder.html?type=Images') }}',
        filebrowserFlashBrowseUrl: '{{ asset('ckfinder/ckfinder.html?type=Flash') }}',
        filebrowserUploadUrl: '{{ asset('ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files') }}',
        filebrowserImageUploadUrl: '{{ asset('ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images') }}',
        filebrowserFlashUploadUrl: '{{ asset('ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash') }}'
    }); </script>
@endsection