@extends('admin.layouts.app')
@section('content')
                <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Table Country</h4>
                                <h6 class="card-subtitle">List country</h6>
                            </div>
                            <div class="table-responsive">
                                <table class="table">
                                    <thead class="thead-light">
                                        <tr>
                                            <th scope="col">#</th>
                                            <th scope="col">Title</th>
                                            <th scope="col">Image</th>
                                            <th scope="col">Description</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    @foreach($data_blog->all() as $value)
                                <tbody>
                                    <tr>
                                        <th scope="row">{{$value->id}}</th>
                                        <td>{{$value->title}}</td>
                                        <td>{{$value->image}}</td>
                                        <td>{{$value->description}}</td>
                                        <td>
                                            <a class="mdi  mdi-library-books"href="{{ url('admin/blog/edit/'.$value->id) }}"> Edit</a>
                                            <a  href="{{ url('admin/blog/delete/'.$value->id) }}"> Delete</a>
                                        </td>
                                    </tr>                    
                                </tbody>
                                @endforeach
                                </table>
                                
                            </div>
                        </div>
                        {{$data_blog->links()}}
                    </div>
                    <div class="form-group">
                        <div class="col-md-12">
                        @if(!empty($successMessage = \Illuminate\Support\Facades\Session::get('success')))
                            {{ $successMessage }}
                        @endif
                        @if($errors->any())
                            @foreach($erors->all() as $error )
                                {{$eror}}
                            @endforeach
                        @endif
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-12">
                            <a href="{{ url('admin/blog/add') }}" class="btn btn-success">Add Blog</a>
                        </div>
                    </div>
@endsection