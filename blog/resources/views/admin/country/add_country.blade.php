@extends('admin.layouts.app')
@section('content')
    <form method="post" class="form-horizontal form-material">
        {{ csrf_field() }}
        <div class="form-group">
            <label class="col-md-12">New Country(*)</label>
            <div class="col-md-12">
                <input name="country_new" type="text" placeholder="Việt Nam"  class="form-control form-control-line">
            </div>
        </div>
        <div class="form-group">
            <div class="col-md-12">
                <p>{{$thongbao}}</p>
            </div>
        </div>
        <div class="form-group">
            <div class="col-md-12">
            @if(!empty($successMessage = \Illuminate\Support\Facades\Session::get('success')))
                {{ $successMessage }}
            @endif                        
            </div>
        </div>
        <div class="form-group">
            @if($errors->any())
                <ul>
                @foreach($errors->all() as $error)
                    <li>{{$error}}</li>
                @endforeach
                </ul>
            @endif
        </div>
        <div class="form-group">
            <div class="col-sm-12">
                <button type="submit" class="btn btn-success">Add Country</button>
            </div>
        </div>
      </form>
@endsection