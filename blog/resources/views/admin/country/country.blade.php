@extends('admin.layouts.app')
@section('content')
                <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Table Country</h4>
                                <h6 class="card-subtitle">List country</h6>
                            </div>
                            <div class="table-responsive">
                                <table class="table">
                                    <thead class="thead-light">
                                        <tr>
                                            <th scope="col">#</th>
                                            <th scope="col">Country</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    @foreach($data_country->all() as $value)
                                    <tbody>
                                        <tr>
                                            <th scope="row">{{$value->id}}</th>
                                            <td>{{$value->name}}</td>
                                            <td><a href="{{ url('/country/delete/'.$value->id) }}"> Delete</a></td>
                                        </tr>
                                    </tbody>
                                    @endforeach
                                </table>
                            </div>
                            {{$data_country->links()}}
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12">
                        @if(!empty($successMessage = \Illuminate\Support\Facades\Session::get('success')))
                            {{ $successMessage }}
                        @endif
                        @if($errors->any())
                            @foreach($erors->all() as $error )
                                {{$eror}}
                            @endforeach
                        @endif
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-12">
                            <a href="{{ url('/country/add') }}" class="btn btn-success">Add Country</a>
                        </div>
                    </div>
@endsection