<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateColumnTableProduct extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('product', function (Blueprint $table) {
            $table->unsignedbigInteger('id_category')->after('price')->nullable(false);
            $table->unsignedbigInteger('id_brand')->after('id_category')->nullable(false);
            $table->Integer('status')->enum(0,1)->after('id_brand')->nullable(false);
            $table->Integer('sale')->after('status')->default(0);
            $table->String('company')->after('sale')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('product', function (Blueprint $table) {
            //
        });
    }
}
