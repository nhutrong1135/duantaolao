<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class product extends Model
{
    protected $table="product";
    protected $fillable = [
        'id'
        ,'id_users'
        ,'name'
        ,'image'
        ,'price'
        ,'id_category'
        ,'id_brand'
        ,'status'
        ,'sale'
        ,'company'
        ,'details'
        ,'created_at'
    ];

    public $timestamps=false;
}
