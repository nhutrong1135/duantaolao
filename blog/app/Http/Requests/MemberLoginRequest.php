<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class MemberLoginRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return   [
            'email'=>'email|required',
            'password'=>'required|max:2048|min:8',
        ];
    }

    public function messages()
    {
        return   [
            'required'=>'Vui lòng nhập :attribute',
            'max'=>':attribute không lớn hơn :max',
            'min'=>':attribute không nhỏ hơn :min',
            'email'=>':attribute không đúng định dạng email',
        ];
    }

    public function attributes()
    {
        return   [
            'email'=>"Email",
            'password'=>"Mật khẩu",
        ];
    }
}
