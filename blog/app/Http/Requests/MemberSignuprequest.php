<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class MemberSignuprequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return   [
            'name'=>'required',
            'email'=>'email|required',
            'password'=>'required|max:2048|min:8',
            'address'=>'required',
            'phone'=>'required',
        ];
    }

    public function messages()
    {
        return   [
            'required'=>':attribute không được để trống',
            'max'=>':attribute không lớn hơn :max',
            'min'=>':attribute không nhỏ hơn :min',
            'email'=>':attribute không đúng định dạng email',
        ];
    }

    public function attributes()
    {
        return   [
            'name'=>"Tên người dùng",
            'email'=>"Email",
            'password'=>"Mật khẩu",
            'address'=>'Địa chỉ',
            'phone'=>'Số điện thoại',
        ];
    }
}
