<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class Newproduct extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return   [
            'name'=>'required',
            'price'=>'required',
            'id_category'=>'required',
            'id_brand'=>'required',
            'sale'=>'integer',
            'company'=>'required',
            'image'=>'required',
            'details'=>'required',
        ];
    }

    public function messages()
    {
        return   [
            'required'=>':attribute không được để trống',
            'max'=>':attribute không lớn hơn :max',
            'integer'=>':attribute phải là số nguyên',
        ];
    }

    public function attributes()
    {
        return   [
            'name'=>"Tên sản phẩm",
            'price'=>"Giá sản phẩm",
            'image'=>"Hình hiển thị",
            'id_category'=>'Tên Category',
            'id_brand'=>'Tên Brand',
            'sale'=>'Phần trăm sale',
            'company'=>'Tên công ty',
            'details'=>'Mô tả',
        ];
    }
}
