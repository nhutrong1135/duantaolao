<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class newblog extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return   [
            'title'=>'required',
            'image'=>'required|image|max:2048',
            'description'=>'required',
        ];
    }

    public function messages()
    {
        return   [
            'required'=>':attribute không được để trống',
            'max'=>':attribute không lớn hơn :max',
            'image'=>':attribute vui lòng chọn hình ảnh',
        ];
    }

    public function attributes()
    {
        return   [
            'title'=>"Tiêu đề blog",
            'image'=>"Hình hiển thị",
            'description'=>"Mô tả tóm tắt",
        ];
    }
}
