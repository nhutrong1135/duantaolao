<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CheckRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return   [
            'name'=>'required',
            'email'=>'required',
            'avatar'=>'mimes:jpg,png,jpeg,gif|max:2048',
            'passwordnewcorfirm'=>'same:passwordnew',
        ];
    }

    public function messages()
    {
        return   [
            'required'=>':attribute không được để trống',
            'max'=>':attribute không lớn hơn :max',
            'mimes'=>'attribute chỉ được chọn ảnh',
            'same'=>':attribute nhập sai',
        ];
    }

    public function attributes()
    {
        return   [
            'name'=>"tên người dùng",
            'email'=>"tuổi người dùng",
            'avatar'=>"avatar người dùng",
            'passwordnewcorfirm'=>"xác nhận mật khẩu",
        ];
    }
}
