<?php

namespace App\Http\Controllers\frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\NewComment;
use App\blog;
use App\users;
use App\rate;
use App\comment;
use Auth;

class BlogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data_blog=blog::orderBy('id', 'desc')->paginate(3);
        return view('frontend/blog/blog',compact('data_blog'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data_blog=blog::with(['comment'=>function($q){
            $q->orderBy('id','asc');
        }])->find($id);
        $next = blog::where('id', '>', $id)->min('id');
        $prev = blog::where('id', '<', $id)->max('id');
        $data_rate=rate::select('rate')->where('id_blog', 'like', $id)->get();
        $sum=0;$times=count($data_rate);
        foreach ($data_rate as $value) {
            $sum+=$value['rate'];
        }
        if($sum!=0){
            (double)$data_rate=$sum/$times;
        }else{
            $data_rate=0;
        }
        return view('frontend/blog/blog single',compact('data_blog','next','prev','data_rate','times'));
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    //hàm này get ajax
    
    //function ajax rate
    public function ajaxRateRequestPost(Request $request)
    {
        $rate = new rate;
        $data_update = $request->all();
        $data_update['id_users']=Auth::id();
        if($rate->create($data_update)){
            return response()->json(['success'=>"Đánh giá thành công!!!"]);
        }else{
            return response()->json(['error'=>"Đánh giá thất bại!!!"]);
        }
    }
    //function comment blog
    public function commentBlog(NewComment $request)
    {
        $comment = new comment;
        $data_update = $request->all();
        $data_update['id_users']=Auth::id();
        $data_update['user_name']=Auth::user()->name;
        $data_update['user_avatar']=Auth::user()->avatar;
        if($comment->create($data_update)){
            return redirect()->back()->with('success',__('bình luận thành công!!!'));
        }else{
            return redirect()->back()->withErrors('bình luận không thành công!!!');
        }       
    }
}
