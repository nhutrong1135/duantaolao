<?php

namespace App\Http\Controllers\frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\MemberSignupRequest;
use App\Http\Requests\MemberLoginRequest;
use Auth;
use App\users;


class UserController extends Controller
{
    public function register()
    {
        return view('frontend/member/register');
    }
    public function login()
    {
        return view('frontend/member/login');
    }
    public function login_done(MemberLoginRequest $request)
    {
        $login=[
            'email'=>$request->email,
            'password'=>$request->password,
            'level'=>0
        ];
        $remember=false;

        if($request->remember_me){
            $remember=true;
        }
        if(Auth::attempt($login,$remember)){
            return redirect('/home/product');
        }else{
            return redirect()->back()->withErrors('Email or password is not corret!!!');
        }

    }
    public function logout () {
        session_start();
        session_destroy();
        //logout user
        auth()->logout();
        // redirect to homepage
        return redirect('/home/product');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(MemberSignupRequest $request)
    {
        $users= new users;

        $data_update=$request->all();
        $data_update['password']=bcrypt($data_update['password']);
        $data_update['level']=0;
        if($users->create($data_update)){
            return redirect()->back()->with('success',__('Đăng ký thành công!!!'));
        }else{
            return redirect()->back()->withErrors('Đăng ký không thành công!!!');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
