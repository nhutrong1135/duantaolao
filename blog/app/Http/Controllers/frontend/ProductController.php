<?php

namespace App\Http\Controllers\frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\NewProduct;
use App\product;
use App\category;
use App\brand;
use Image;
use Auth;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data_product=product::where('id_users',Auth::id())->get();
        for($i=0; $i<count($data_product);$i++){
            $data_product[$i]['image']=json_decode($data_product[$i]['image']);
        }
        return view("/frontend/product/product",compact('data_product'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data_category=category::all();
        $data_brand=brand::all();
        return view("/frontend/product/add",compact("data_category","data_brand"));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(NewProduct $request)
    {
        $data_update=$request->all();
        if(count($data_update['image'])<=3){
            
            $product= new product ;
            if($request->hasfile('image'))
            {
                $data=$this->processImage($request->image);
            }

            $data_update['id_users']= Auth::id();
            $data_update['image']=json_encode($data);
            if($product->create($data_update)){
                return redirect()->back()->with('success',__('thêm thành công!!!'));
            }else{
                return redirect()->back()->withErrors('thêm thất bại!!!');
            }
        }
        else{
            
            return redirect()->back()->withErrors('Chỉ thêm nhiều nhất 3 hình cho 1 product!!!');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data_product=product::where('id',$id)->get();
        $data_category=category::all();
        $data_brand=brand::all();
        $data_product[0]['image']=json_decode($data_product[0]['image']);
        return view("/frontend/product/edit",compact('data_product',"data_category","data_brand"));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request ,$id)
    {
        $data = [];
        $data_update=$request->all();
        //khúc này là để lấy image trong mảng và xóa những image được đánh dấu
        $data_product=product::where('id',$id)->get();
        $data_product=json_decode($data_product[0]['image']); 
        $count_data_product=count($data_product);

        //kiểm tra xem có hình muốn xóa hay không
        if(!empty($data_update['image_delete'])){
            for($i=0;$i<$count_data_product;$i++){
                if(in_array($data_product[$i],$data_update['image_delete'])){
                    unset($data_product[$i]); 
                }
            }
        }

        //lấy image và cắt ra nhiều kích cỡ khác nhau
        if($request->hasfile('image'))
            {
                $data=$this->processImage($request->image);
            }

        // tạo mảng hình ảnh mới bởi hình mới cũ và hình mới thêm vào
        $image_new=array_merge($data_product,$data);

        //kiểm tra mảng mới có nhiều hơn 3 hình không rồi cho phép thêm vào
        if(count($image_new)<=3){
            
            $product=product::findOrFail($id);
            

            $data_update['id_users']= Auth::id();
            $data_update['image']=json_encode($image_new);
            if($product->update($data_update)){
                return redirect()->back()->with('success',__('Chỉnh sửa thành công!!!'));
            }else{
                return redirect()->back()->withErrors('Chỉnh sửa thất bại!!!');
            }
        }
        else{       
            return redirect()->back()->withErrors('Chỉ cho phép nhiều nhất 3 hình cho 1 product!!!');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product=product::where("id",$id);
        if($product->delete()){
            return redirect()->back()->with('success',__('xóa blog thành công!!!'));
        }else{
            return redirect()->back()->withErrors('xóa blog không thành công!!!');
        }
    }
    //ham xử lý ảnh
    public function processImage($image_product){
        $data = [];
        $time=strtotime(date('Y-m-d H:i:s'));
        foreach($image_product as $image){
                    $name = $time.$image->getClientOriginalName();
                    $name_2 = "hinh50_".$time.$image->getClientOriginalName();
                    $name_3 = "hinh200_".$time.$image->getClientOriginalName();

                    //$image->move('upload/product/', $name);
                    if(!is_dir('upload/user/product/')){
                        mkdir('upload/user/product/');
                    }
                    $path = public_path('upload/user/product/' . $name);
                    $path2 = public_path('upload/user/product/' . $name_2);
                    $path3 = public_path('upload/user/product/' . $name_3);

                    Image::make($image->getRealPath())->save($path);
                    Image::make($image->getRealPath())->resize(50, 70)->save($path2);
                    Image::make($image->getRealPath())->resize(200, 300)->save($path3);
                    
                    $data[] = $name;
                }
        return $data;
    }

    //show product details
    public function detail($id)
    {
        $data_product=product::where('id',$id)->get();
        $data_product[0]['image']=json_decode($data_product[0]['image']);
        return view('frontend/product/detail',compact('data_product'));
    }
}
