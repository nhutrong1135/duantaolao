<?php

namespace App\Http\Controllers\frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\product;
use App\brand;
use App\category;

class HomeController extends Controller
{
    public function search_name(Request $request){
        $data_product=product::where('name','like','%'.$request->name.'%')->orderBy('id', 'desc')->paginate(6);
        for($i=0; $i<count($data_product);$i++){
            $data_product[$i]['image']=json_decode($data_product[$i]['image']);
        }
        $data_category=category::all();
        $data_brand=brand::all();
        return view('/frontend/home/index',compact('data_product','data_brand','data_category'));
    }

    public function index_search()
    {
        $data_product=product::orderBy('id', 'desc')->paginate(6);
        for($i=0; $i<count($data_product);$i++){
            $data_product[$i]['image']=json_decode($data_product[$i]['image']);
        }
        $data_category=category::all();
        $data_brand=brand::all();
        return view('/frontend/home/search',compact('data_product','data_brand','data_category'));
    }

    public function search(Request $request){
        $data_product = product::query();

        if (!empty($request->name)) {
            $data_product = $data_product->where('name','like','%'.$request->name.'%');
        }

        if (!empty($request->price)) {
            $price = explode("-", $request->price);
            $data_product = $data_product->whereBetween('price', [(int)$price[0], (int)$price[1]]);;
        }

        if (!empty($request->id_brand)) {
            $data_product = $data_product->where('id_brand',$request->id_brand);
        }
        
        if (!empty($request->status)) {
            if($request->status==2)
                $request->status=0;
            $data_product = $data_product->where('status',(int)$request->status);
        }


        $data_product = $data_product->get();
        for($i=0; $i<count($data_product);$i++){
            $data_product[$i]['image']=json_decode($data_product[$i]['image']);
        }
        $data_category=category::all();
        $data_brand=brand::all();
        return view('/frontend/home/search',compact('data_product','data_brand','data_category'));
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data_product=product::orderBy('id', 'desc')->paginate(6);
        for($i=0; $i<count($data_product);$i++){
            $data_product[$i]['image']=json_decode($data_product[$i]['image']);
        }
        return view('/frontend/home/index',compact('data_product'));
    }

    public function ajaxSearchRange(Request $request){
        // echo $request;
        $data_product = product::whereBetween('price', [(int)$request->min, (int)$request->max])->get();
        for($i=0; $i<count($data_product);$i++){
            $data_product[$i]['image']=json_decode($data_product[$i]['image']);
        }
        return response()->json(['success'=>"thành công!!!",'data'=>$data_product]);

    }
}
