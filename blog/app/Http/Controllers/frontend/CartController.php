<?php

namespace App\Http\Controllers\frontend;
use Illuminate\Support\Facades\Mail;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\MemberSignupRequest;
use App\product;
use App\cart;
use App\users;
use App\history;
use App\Mail\SendOrder;
use Auth;

class CartController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        session_start();
        $data_cart=[];
        if(isset($_SESSION["cart"])){
            $data_cart=$_SESSION["cart"];
        }
        return view("frontend/cart/cart",compact('data_cart'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
 
    public function checkout(MemberSignupRequest $request)
    {    
        session_start();

       if(!Auth::check()){
            $users= new users;

            $data_update=$request->all();
            $data_update['password']=bcrypt($data_update['password']);
            $data_update['level']=0;
            if($users->create($data_update)){
                $login=[
                    'email'=>$request->email,
                    'password'=>$request->password,
                    'level'=>0
                ];
                Auth::attempt($login);
            }else{
                return redirect()->back()->withErrors('Đăng ký không thành công!!!');
            }
       }

       if(Auth::check()){
            $this->sendMail();
            return redirect()->back()->with('success',__('Order thành công!!!'));
       }

        
    }
    public function sendMail(){
        $cart = $_SESSION["cart"];

            foreach ($cart as $key => $value) {
                $data_cart[$key] = product::find($value[0]->id)->toArray();

                $data_cart[$key]['qty'] = $value[0]->qty;
            
            }
            
            $emailTo = Auth::user()->email;
            $sum=0;
            foreach ($data_cart as $key => $value) {
                $sum+=((int)$value['price'] - (int)$value['price']*$value['sale']/100)*$value['qty'];
            }
            Mail::send('frontend.sendMail.Order',
                array(
                    'data_cart'=>$data_cart,
                    'sum'=>$sum,
                ),

                function ($message) use ( $emailTo){
                    $message->from('nguyennhutrong69@gmail.com', 'Mail order product');
                    $message->to($emailTo);
                    $message->subject('Mail confirm order shopee');
            });

            $history=new history;
            $data_user=users::select('email','phone','name')->where('id',Auth::id())->get();
            $data_update['id_users']=Auth::id();
            $data_update['price'] = $sum;
            $data_update['name'] =$data_user[0]->name;
            $data_update['phone'] =$data_user[0]->phone;
            $data_update['email'] =$data_user[0]->email;

            if($history->create($data_update)){
                unset($_SESSION['cart']);
            }
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        session_start();
        $data_product=product::where('id',$request->id_product)->get();
        $data_product[0]['image']=json_decode($data_product[0]['image']);
        $data_product[0]['qty']=1;
        $check=0;
        if($data_product!=null){
            //kiểm tra xem có session tồn tại k
            if(isset($_SESSION["cart"])){
                //kiểm tra nếu sản phẩm có tồn tại trong cart chưa nếu có chỉ tăng thêm số lượng
                foreach($_SESSION["cart"] as $values){
                    if($values[0]->id==$request->id_product){
                        $_SESSION["cart"][$request->id_product][0]->qty=(int)$values[0]->qty+1;
                        $check=1;
                    }
                }
            }
            if($check==0){
                $_SESSION["cart"][$request->id_product]=$data_product;
            }
            $data_cart=$_SESSION["cart"];
            return response()->json(['success'=>"Thêm vào giỏ hàng thành công!!!",'data'=>$data_cart]);
        }else{
            return response()->json(['error'=>"Thêm vào giỏ hàng thất bại!!!"]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function ajaxQtyPost(Request $request)
    {
        session_start();
        $cart=$_SESSION["cart"][$request->id_product];
        if($cart[0]->qty>1){
            $_SESSION["cart"][$request->id_product][0]->qty=$request['qty'];
        }
        if($cart[0]->qty==1||$request['qty']==0){
            unset($_SESSION["cart"][$request->id_product]);
        }
        return response()->json(['success'=>"Thành công!!!"]);
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

}
