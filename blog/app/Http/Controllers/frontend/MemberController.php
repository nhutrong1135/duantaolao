<?php

namespace App\Http\Controllers\frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\CheckRequest;
use App\country;
use App\users;
use Auth;
class MemberController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth');

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data_country=country::all();
        return view("/frontend/member/profile",compact("data_country"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     * 
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CheckRequest $request)
    {
        $userId = Auth::id();
        $user=users::findOrFail($userId);

        $data_update=$request->all();
        $file=$request->avatar;

        if(!empty($file)){
            $data_update["avatar"]=$file->getClientOriginalName();
        }
        if($data_update["passwordnew"]){
            $data_update["passwordnew"] =bcrypt($data_update["passwordnew"]);
        }else{
            $data_update["passwordnew"]=$user->password;
        }
        if($user->update($data_update)){
            if(!empty($file)){
                $file->move('upload/user/avatar',$file->getClientOriginalName());
            }
            return redirect()->back()->with('success',__('cập nhật thành công!!!'));
        }else{
            return redirect()->back()->withErrors('cập nhật thất bại!!!');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
