<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('admin/user/dashboard');
    }
    public function logout () {
        session_start();
        session_destroy();
        //logout user
        auth()->logout();
        // redirect to homepage
        return redirect('/');
    }
    
}
