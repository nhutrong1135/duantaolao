<?php

namespace App\Http\Controllers\Admin;
use App\Http\Requests\New_Country;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\country;

class CountryController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }


    public function index()
    {
        $data_country=country::simplePaginate(5);
        return view('admin/country/country',compact("data_country"));
    }
    // -----------------------------------------------------------------------
    public function create()
    {
        $thongbao="";
        return view("admin/country/add_country",compact("thongbao"));
    }
     // -----------------------------------------------------------------------   
    public function store(New_Country $request)
    {
        $table = new country();
            $table->name=$request->country_new;
            $table->save();
        $thongbao=" Thêm thành công!!!";
        return view("admin/country/add_country",compact("thongbao"));
    }
    // -----------------------------------------------------------------------
    public function destroy($id)
    {
        $country=country::where("id",$id);
        if($country->delete()){
            return redirect()->back()->with('success',__('xóa country thành công!!!'));
        }else{
            return redirect()->back()->withErrors('xóa country không thành công!!!');
        }
    }
}
