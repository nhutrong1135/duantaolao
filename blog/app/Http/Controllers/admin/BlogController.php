<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\newblog;
use App\Http\Requests\edit_blog;
use App\blog;
class BlogController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    
    public function index()
    {
        $data_blog=blog::paginate(3);
        return view("admin/blog/blog",compact("data_blog"));
        // return view('admin/user/country',compact("data_country"));
    }
    // -----------------------------------------------------------------------
    public function create()
    {
        return view("admin/blog/add_blog");
    }
    // -----------------------------------------------------------------------
    public function store(newblog $request)
    {
        // echo $request->input("content");
        $blog= new blog;

        $data_update=$request->all();
        $file=$request->image;

        if(!empty($file)){
            $data_update["image"]=$file->getClientOriginalName();
        }
        if($blog->create($data_update)){

            if(!empty($file)){
                $file->move('upload/user/image',$file->getClientOriginalName());
            }
            
            return redirect()->back()->with('success',__('thêm thành công!!!'));
        }else{
            return redirect()->back()->withErrors('thêm không thành công!!!');
        }
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    // -----------------------------------------------------------------------
    public function show_edit_blog($id)
    {
        $data_1blog=blog::where("id",$id)->get();
        return view("admin/blog/edit_blog",compact("data_1blog"));
    }

    // -----------------------------------------------------------------------
    public function update(edit_blog $request,$id)
    {
        $blog=blog::findOrFail($id);

        $data_update=$request->all();
        $file=$request->image;

        if(!empty($file)){
            $data_update["image"]=$file->getClientOriginalName();
        }
        if($blog->update($data_update)){
            
            if(!empty($file)){
                $file->move('upload/user/image',$file->getClientOriginalName());
            }

            return redirect()->back()->with('success',__('cập nhật blog thành công!!!'));
        }else{
            return redirect()->back()->withErrors('cập nhật blog không thành công!!!');
        }
    }
    // -----------------------------------------------------------------------
    public function destroy($id){
        $blog=blog::where("id",$id);
        if($blog->delete()){
            return redirect()->back()->with('success',__('xóa blog thành công!!!'));
        }else{
            return redirect()->back()->withErrors('xóa blog không thành công!!!');
        }
    }
}
