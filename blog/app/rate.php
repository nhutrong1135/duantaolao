<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class rate extends Model
{
    protected $table="rate";
    protected $fillable = ['id','id_users','id_blog','rate'];
    public $timestamps=false;
}
