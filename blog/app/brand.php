<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class brand extends Model
{
    protected $table="brand";

    protected $fillable = [
        'id','brand'
    ];

    public $timestamps=false;
}
