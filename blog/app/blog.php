<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class blog extends Model
{
    protected $table="blog";

    protected $fillable = [
        'id','title','image','description','content'
    ];

    public $timestamps=false;

    public function comment(){
        return $this->hasMany('App\comment','id_blog');
    }
}
