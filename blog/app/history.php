<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class history extends Model
{
    protected $table="history";
    protected $fillable = ['id','id_users','email','phone','name','price'];
    public $timestamps=false;
}
