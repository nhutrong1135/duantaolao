<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class users extends Model
{
    protected $fillable = ['id','name','email','password','phone','address','country','avatar','level'];
    public $timestamps="false";
}
