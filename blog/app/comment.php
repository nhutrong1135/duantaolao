<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class comment extends Model
{
    protected $table="comments";
    protected $fillable = ['id','id_users','id_blog','user_name', 'user_avatar', 'content', 'id_cmt_cha', 'created_at'];
}
