<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Admin\Controller;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/logout', 'HomeController@logout');

Route::group([
    'namespace'=>'frontend'
],function(){
    
    Route::get('/home/product', 'HomeController@index');

    Route::get('/blog', 'BlogController@index');
    Route::get('/blog/single/{id}', 'BlogController@show');
    Route::post('/blog/ajaxRate', 'BlogController@ajaxRateRequestPost')->name('ajaxRateRequest.post');
    Route::post('/blog/comment', 'BlogController@commentBlog');

    Route::get('/member/login', 'UserController@login');
    Route::post('/member/login', 'UserController@login_done');
    Route::get('/member/logout', 'UserController@logout');
    Route::get('/member/register', 'UserController@register');
    Route::post('/member/register', 'UserController@store');
    //edit profile member
    Route::get('/profile', 'MemberController@index');
    Route::post('/profile', 'MemberController@update');

    //product
    Route::get('/account/product', 'ProductController@index');
    Route::get('/account/product/add', 'ProductController@create');
    Route::post('/account/product/add', 'ProductController@store');
    Route::get('/account/product/edit/{id}', 'ProductController@show');
    Route::post('/account/product/edit/{id}', 'ProductController@update');
    Route::get('/account/product/delete/{id}', 'ProductController@destroy');

    Route::get('/product/detail/{id}', 'ProductController@detail');
    //add and up down qty cart
    Route::post('/product/ajaxCart/add', 'CartController@store')->name('ajaxAddCartRequest.post');
    Route::post('/product/ajaxCart/up_donwn_qty','CartController@ajaxQtyPost')->name('ajaxQty.post');
    //search
    Route::post('/product/search-name', 'HomeController@search_name');
    Route::get('/product/search', 'HomeController@index_search');
    Route::post('/product/search', 'HomeController@search');
    Route::post('/product/search-range','HomeController@ajaxSearchRange')->name('ajaxSearch.post');

    //show cart and checkout
    Route::get('/cart', 'CartController@index');
    Route::post('/cart/order', 'CartController@checkout');

});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group([
    'prefix'=>'admin',
    'namespace'=>'admin',
],function(){

    Route::get('/profile', 'UserController@index');
    Route::post('/profile', 'UserController@store');

    Route::get('/country', 'CountryController@index');
    Route::get('/country/add', 'CountryController@create');
    Route::post('/country/add', 'CountryController@store');
    Route::get('/country/delete/{id}', 'CountryController@destroy');

    Route::get('/blog', 'BlogController@index');
    Route::get('/blog/add', 'BlogController@create');
    Route::post('/blog/add', 'BlogController@store');
    Route::get('/blog/edit/{id}', 'BlogController@show_edit_blog');
    Route::post('/blog/edit/{id}', 'BlogController@update');
    Route::get('/blog/delete/{id}', 'BlogController@destroy');
});